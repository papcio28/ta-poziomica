package com.akademiakodu.poziomica;

import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements SensorEventListener {
    @BindView(R.id.dot)
    protected View mDot;
    @BindView(R.id.background)
    protected FrameLayout mBackground;

    private SensorManager sensorManager;
    private DisplayMetrics displayMetrics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensorManager.registerListener(this,
                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_FASTEST);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) mDot.getLayoutParams();

        float leftMargin = (event.values[0] / 10.0f) * displayMetrics.widthPixels;
        float bottomMargin = (event.values[1] / 10.0f) * displayMetrics.heightPixels;

        double distance = Math.sqrt(Math.pow(event.values[0], 2)
                + Math.pow(event.values[1], 2));

        mBackground.setBackgroundColor(Color.argb((int) ((distance / 10) * 255), 0, 255, 0));

        params.setMargins((int) leftMargin, 0, 0, (int) bottomMargin);

        mDot.setLayoutParams(params);
    }

    private String printFloatArray(float[] values) {
        StringBuilder valuesString = new StringBuilder();
        for (float val : values) {
            valuesString.append(val).append(" ");
        }
        return valuesString.toString();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
